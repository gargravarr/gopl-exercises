// Mandelbrot emits a PNG image of the Mandelbrot fractal.
package main

import (
	"errors"
	"flag"
	"fmt"
	"image"
	"image/color"
	"image/png"
	"math/big"
	"math/cmplx"
	"os"
)

type numOpt string

var (
	num     = numOpt("complex128")
	numVals = []string{"complex64", "complex128", "bigfloat", "bigrat"}

	zoom    = flag.Uint("zoom", 1, "Zoom factor")
	halfCpx = flag.Bool("half-complex", false,
		"Use complex64 instead of complex128")
)

func init() {
	flag.Var(&num, "num", fmt.Sprintf("Number type: %v", numVals))
	flag.Parse()
}

func (n *numOpt) String() string {
	return string(*n)
}

func (n *numOpt) Set(s string) error {
	for _, val := range numVals {
		if s == val {
			*n = numOpt(s)
			return nil
		}
	}

	return errors.New(fmt.Sprintf("Error: must be one of %v", numVals))
}

func main() {
	const (
		xmin, ymin, xmax, ymax = -2, -2, +2, +2
	)

	var width, height int = 1024 * int(*zoom), 1024 * int(*zoom)

	img := image.NewRGBA(image.Rect(0, 0, width, height))
	for py := 0; py < height; py++ {
		y := float64(py)/float64(height)*(ymax-ymin) + ymin
		for px := 0; px < width; px++ {
			x := float64(px)/float64(width)*(xmax-xmin) + xmin

			switch num {
			case "complex64":
				z := complex(x, y)
				img.Set(px, py, mandelbrot64(complex64(z)))
			case "complex128":
				z := complex(x, y)
				img.Set(px, py, mandelbrot(z))
			case "bigfloat":
				img.Set(px, py, mandelbrotBigFloat(big.NewFloat(x),
					big.NewFloat(y)))
			}
		}
	}

	png.Encode(os.Stdout, img) // NOTE: ignoring errors
}

func mandelbrot(z complex128) color.Color {
	const iterations = 200
	const contrast = 15

	var v complex128
	for n := uint8(0); n < iterations; n++ {
		v = v*v + z
		if cmplx.Abs(v) > 2 {
			return color.Gray{255 - contrast*n}
		}
	}
	return color.Black
}

func mandelbrot64(z complex64) color.Color {
	const iterations = 200
	const contrast = 15

	var v complex64
	for n := uint8(0); n < iterations; n++ {
		v = v*v + z
		if cmplx.Abs(complex128(v)) > 2 {
			return color.Gray{255 - contrast*n}
		}
	}
	return color.Black
}

func mandelbrotBigFloat(x, y *big.Float) color.Color {
	const iterations = 200
	const contrast = 15

	vx := big.NewFloat(0)
	vy := big.NewFloat(0)
	square := big.NewFloat(4)
	for n := uint8(0); n < iterations; n++ {
		fmt.Println(n, x, y)
		xTemp := new(big.Float).Sub(new(big.Float).Mul(vx, vx),
			new(big.Float).Mul(vy, vy))
		xTemp.Add(xTemp, x)

		//		vy = new(big.Float).Add(new(big.Float).Mul(new(big.Float).Mul(vx, vy),
		//			big.NewFloat(2)), vy)
		vx = xTemp

		target := new(big.Float).Add(new(big.Float).Mul(vx, vx),
			new(big.Float).Mul(vx, vx))
		if square.Cmp(target) > 0 {
			return color.Gray{255 - contrast*n}
		}
	}
	return color.Black
}
