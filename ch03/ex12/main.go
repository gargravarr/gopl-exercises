package main

import (
	"fmt"
	"os"
)

func main() {
	if len(os.Args) < 3 {
		fmt.Fprintf(os.Stderr, "Not enough arguments, minimum 2.\n")
		return
	}

	fmt.Printf("%t\n", anagram(os.Args[1], os.Args[2]))
}

// comma inserts commas in a number string.
func anagram(s1, s2 string) bool {
	if len(s1) != len(s2) {
		return false
	}

	seen := []int{}
	for _, r1 := range s1 {
		matches := false

		for i2, r2 := range s2 {
			if r1 == r2 {
				matches = true

				wasSeen := false
				for _, index := range seen {
					if i2 == index {
						wasSeen, matches = true, false
						break
					}
				}

				if !wasSeen {
					seen = append(seen, i2)
					break
				}
			}
		}

		if !matches {
			return false
		}
	}

	return true
}
