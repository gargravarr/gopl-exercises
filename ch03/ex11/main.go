package main

import (
	"bytes"
	"fmt"
	"os"
	"strings"
)

func main() {
	for i := 1; i < len(os.Args); i++ {
		fmt.Printf("  %s\n", comma(os.Args[i]))
	}
}

// comma inserts commas in a number string.
func comma(s string) string {
	sep := ','
	var buf bytes.Buffer

	if s[0] == '+' || s[0] == '-' {
		buf.WriteByte(s[0])
		s = s[1:]
	}

	dec := ""
	if dot := strings.Index(s, "."); dot >= 0 {
		dec = s[dot:]
		s = s[:dot]
	}

	for i := 0; i < len(s); {
		if len(s[i:])%3 != 0 {
			buf.WriteString(s[:len(s)%3])
			i = len(s) % 3
		} else {
			buf.WriteString(s[i : i+3])
			i += 3
		}

		if i < len(s) {
			buf.WriteRune(sep)
		}
	}

	if len(dec) > 0 {
		buf.WriteString(dec)
	}

	return buf.String()
}
