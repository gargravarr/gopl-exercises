// Mandelbrot emits a PNG image of the Mandelbrot fractal.
package main

import (
	"image"
	"image/color"
	"image/png"
	"math/cmplx"
	"os"
)

const (
	xmin, ymin, xmax, ymax = -2, -2, +2, +2
	width, height          = 1024, 1024
)

func main() {
	img := image.NewRGBA(image.Rect(0, 0, width, height))

	for py := 0; py < height; py++ {
		for px := 0; px < width; px++ {
			// Image point (px, py) represents complex value z.
			img.Set(px, py, supersample(px, py))
		}
	}

	png.Encode(os.Stdout, img) // NOTE: ignoring errors
}

func supersample(px, py int) color.Color {
	colors := []color.YCbCr{}
	for subpy := float64(py); subpy < float64(py+1.0); subpy += .5 {
		y := subpy/height*(ymax-ymin) + ymin
		for subpx := float64(px); subpx < float64(px+1.0); subpx += .5 {
			x := float64(subpx)/width*(xmax-xmin) + xmin
			z := complex(x, y)

			if clr, ok := mandelbrot(z).(color.YCbCr); ok {
				colors = append(colors, clr)
			} else {
				return clr
			}
		}
	}

	var y, cb, cr uint8
	for _, c := range colors {
		y += c.Y
		cb += c.Cb
		cr += c.Cr
	}

	l := uint8(len(colors))
	return color.YCbCr{y / l, cb / l, cr / l}
}

func mandelbrot(z complex128) color.Color {
	const iterations = 200
	const contrast = 15

	var v complex128
	for n := uint8(0); n < iterations; n++ {
		v = v*v + z
		if cmplx.Abs(v) > 2 {
			blue := uint8(real(v)*128) + 127
			red := uint8(imag(v)*128) + 127
			return color.YCbCr{192, blue, red}
		}
	}
	return color.Black
}
