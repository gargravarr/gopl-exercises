// Surface computes an SVG rendering of a 3-D surface function.
package main

import (
	"fmt"
	"log"
	"math"
	"net/http"
	"strconv"
)

const (
	cells   = 100         // number of grid cells
	xyrange = 30.0        // axis ranges (-xyrange..+xyrange)
	angle   = math.Pi / 6 // angle of x, y axes (=30°)
)

var (
	sin30, cos30 = math.Sin(angle), math.Cos(angle) // sin(30°), cos(30°)
)

func main() {
	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe("localhost:8000", nil))
}

func handler(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		log.Fatal(err)
	}

	params := map[string]string{
		"color":  "grey",
		"width":  "600",
		"height": "320",
	}

	for k, v := range r.Form {
		params[k] = v[0]
	}

	width, err := strconv.Atoi(params["width"])
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Printf("Error: %v", err)
		return
	}

	height, err := strconv.Atoi(params["height"])
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Printf("Error: %v", err)
		return
	}

	w.Header().Set("Content-Type", "image/svg+xml")
	fmt.Fprintf(w, "<svg xmlns='http://www.w3.org/2000/svg' "+
		"style='stroke: %s; fill: white; stroke-width: 0.7' "+
		"width='%d' height='%d'>", params["color"], width, height)

	for i := 0; i < cells; i++ {
		for j := 0; j < cells; j++ {
			ax, ay, oka := corner(i+1, j, width, height)
			bx, by, okb := corner(i, j, width, height)
			cx, cy, okc := corner(i, j+1, width, height)
			dx, dy, okd := corner(i+1, j+1, width, height)
			if !oka || !okb || !okc || !okd {
				continue
			}

			fmt.Fprintf(w, "<polygon points='%g,%g %g,%g %g,%g %g,%g'/>\n",
				ax, ay, bx, by, cx, cy, dx, dy)
		}
	}
	fmt.Fprintf(w, "</svg>")
}

func corner(i, j, width, height int) (float64, float64, bool) {
	xyscale := width / 2 / xyrange  // pixels per x or y unit
	zscale := float64(height) * 0.4 // pixels per z unit

	// Find point (x,y) at corner of cell (i,j).
	x := xyrange * (float64(i)/cells - 0.5)
	y := xyrange * (float64(j)/cells - 0.5)

	// Compute surface height z.
	z, ok := f(x, y)
	if !ok {
		return .0, .0, false
	}

	// Project (x,y,z) isometrically onto 2-D SVG canvas (sx,sy).
	sx := float64(width)/2 + (x-y)*cos30*float64(xyscale)
	sy := float64(height)/2 + (x+y)*sin30*float64(xyscale) - z*zscale
	return sx, sy, true
}

func f(x, y float64) (float64, bool) {
	r := math.Hypot(x, y) // distance from (0,0)

	res := math.Sin(r) / r
	if math.IsNaN(r) {
		return .0, false
	}

	return res, true
}
