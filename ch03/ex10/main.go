package main

import (
	"bytes"
	"fmt"
	"os"
)

func main() {
	for i := 1; i < len(os.Args); i++ {
		fmt.Printf("  %s\n", comma(os.Args[i]))
	}
}

// comma inserts commas in a non-negative decimal integer string.
func comma(s string) string {
	sep := ','
	var buf bytes.Buffer

	for i := 0; i < len(s); {
		if len(s[i:])%3 != 0 {
			buf.WriteString(s[:len(s)%3])
			i = len(s) % 3
		} else {
			buf.WriteString(s[i : i+3])
			i += 3
		}

		if i < len(s) {
			buf.WriteRune(sep)
		}
	}
	return buf.String()
}
