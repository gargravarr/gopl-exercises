// Ex04 prints the count and text of lines that appear more than once
// in the input.  It reads from stdin or from a list of named files.
package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	counts := make(map[string]int)
	filesWithLine := make(map[string]string)

	if files := os.Args[1:]; len(files) == 0 {
		countLines(os.Stdin, "<stdin>", counts, filesWithLine)
	} else {
		for _, arg := range files {
			f, err := os.Open(arg)
			if err != nil {
				fmt.Fprintf(os.Stderr, "dup2: %v\n", err)
				continue
			}
			countLines(f, arg, counts, filesWithLine)
			f.Close()
		}
	}

	for line, n := range counts {
		if n > 1 {
			fmt.Printf("%d%s\t%s\n", n, filesWithLine[line], line)
		}
	}
}

func countLines(f *os.File, filename string, counts map[string]int,
	filesWithLine map[string]string) {

	fileCounts := make(map[string]int)

	// NOTE: ignoring potential errors from input.Err()
	for input := bufio.NewScanner(f); input.Scan(); {
		fileCounts[input.Text()]++
	}

	for line, n := range fileCounts {
		counts[line] += n
		filesWithLine[line] += " " + filename
	}
}
