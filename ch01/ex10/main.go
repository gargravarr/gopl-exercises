// Fetchall fetches URLs in parallel and reports their times and sizes to the
// file specified in the first parameter.
package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

func main() {
	f, err := os.Create(os.Args[1])
	if err != nil {
		fmt.Fprintf(os.Stderr, "error: %v", err)
	}

	start := time.Now()

	ch := make(chan string)
	for _, url := range os.Args[2:] {
		go fetch(url, ch) // start a goroutine
	}

	for range os.Args[2:] {
		if _, err := f.WriteString(<-ch + "\n"); err != nil {
			fmt.Fprintf(os.Stderr, "While writing to %s: %v", os.Args[1], err)
		}
	}

	elapsed := fmt.Sprintf("%.2fs elapsed\n", time.Since(start).Seconds())
	if _, err := f.WriteString(elapsed); err != nil {
		fmt.Fprintf(os.Stderr, "While writing to %s: %v", os.Args[1], err)
	}

	f.Close()
}

func fetch(url string, ch chan<- string) {
	start := time.Now()
	resp, err := http.Get(url)
	if err != nil {
		ch <- fmt.Sprint(err) // send to channel ch
		return
	}

	nbytes, err := io.Copy(ioutil.Discard, resp.Body)
	resp.Body.Close() // don't leak resources
	if err != nil {
		ch <- fmt.Sprintf("while reading %s: %v", url, err)
		return
	}
	secs := time.Since(start).Seconds()
	ch <- fmt.Sprintf("%.2fs  %7d  %s", secs, nbytes, url)
}
