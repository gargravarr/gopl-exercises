// Lissajous generates GIF animations of random Lissajous figures.
package main

import (
	"fmt"
	"image"
	"image/color"
	"image/gif"
	"io"
	"log"
	"math"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"time"
)

//!+main

var palette = []color.Color{color.White, color.Black}

const (
	whiteIndex = 0 // first color in palette
	blackIndex = 1 // next color in palette
)

func main() {
	rand.Seed(time.Now().UTC().UnixNano())

	// run a web server
	if len(os.Args) > 1 && os.Args[1] == "web" {
		http.HandleFunc("/", handler)
		log.Fatal(http.ListenAndServe("localhost:8000", nil))
		return
	}

	lissajous(os.Stdout, map[string]string{})
}

func handler(w http.ResponseWriter, r *http.Request) {

	if err := r.ParseForm(); err != nil {
		log.Print(err)
	}

	params := make(map[string]string)
	for k, v := range r.Form {
		params[k] = v[0]
	}

	lissajous(w, params)
}

func lissajous(out io.Writer, params map[string]string) {

	values := map[string]string{
		"cycles":  "5",
		"res":     "0.001",
		"size":    "100",
		"nframes": "64",
		"delay":   "8",
	}

	for k, v := range params {
		values[k] = v
	}

	cycles, err := strconv.Atoi(values["cycles"])
	res, err := strconv.ParseFloat(values["res"], 64)
	size, err := strconv.Atoi(values["size"])
	nframes, err := strconv.Atoi(values["nframes"])
	delay, err := strconv.Atoi(values["delay"])
	if err != nil {
		fmt.Fprintf(out, "Error: %v", err)
		return
	}

	freq := rand.Float64() * 3.0 // relative frequency of y oscillator
	anim := gif.GIF{LoopCount: nframes}
	phase := 0.0 // phase difference
	for i := 0; i < nframes; i++ {
		rect := image.Rect(0, 0, 2*size+1, 2*size+1)
		img := image.NewPaletted(rect, palette)
		for t := 0.0; t < float64(cycles)*2*math.Pi; t += res {
			x := math.Sin(t)
			y := math.Sin(t*freq + phase)
			img.SetColorIndex(size+int(x*float64(size)+0.5),
				size+int(y*float64(size)+0.5), blackIndex)
		}
		phase += 0.1
		anim.Delay = append(anim.Delay, delay)
		anim.Image = append(anim.Image, img)
	}
	gif.EncodeAll(out, &anim) // NOTE: ignoring encoding errors
}
