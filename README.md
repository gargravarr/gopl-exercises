# Solutions To _The Go Programming Language_

Here you can find my solutions to the exercises of the book
[The Go Programming Language].

[The Go Programming Language]: https://www.gopl.io/
