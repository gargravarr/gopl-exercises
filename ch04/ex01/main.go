package main

import (
	"crypto/sha256"
	"fmt"
)

func main() {
	c1 := sha256.Sum256([]byte("a"))
	c2 := sha256.Sum256([]byte("b"))
	fmt.Printf("%x\n%x\n%t\n%T\n", c1, c2, c1 == c2, c1)

	diff1 := differentBits(c1, c2)
	diff2 := differentBits(c2, c2)
	fmt.Printf("diff c1 c2: %d\n", diff1)
	fmt.Printf("diff c2 c2: %d\n", diff2)
}

func differentBits(a1, a2 [sha256.Size]byte) int {
	n := 0
	for i, b := range a1 {
		for j := 0; j < 8; j++ {
			if b&1 != a2[i]&1 {
				n++
			}
			b >>= 1
			a2[i] >>= 1
		}
	}

	return n
}
