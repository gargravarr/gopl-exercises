package main

import (
	"bufio"
	"crypto/sha256"
	"crypto/sha512"
	"flag"
	"fmt"
	"os"
)

var sha = flag.String("sha", "256", "SHA hash - 256, 384, or 512")

func main() {
	flag.Parse()

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		switch *sha {
		case "256":
			fmt.Printf("%x\n", sha256.Sum256(scanner.Bytes()))
		case "384":
			fmt.Printf("%x\n", sha512.Sum384(scanner.Bytes()))
		case "512":
			fmt.Printf("%x\n", sha512.Sum512(scanner.Bytes()))
		default:
			fmt.Fprintf(os.Stderr, "Wrong SHA hash.\n")
			os.Exit(1)
		}

		if err := scanner.Err(); err != nil {
			fmt.Fprintf(os.Stderr, "Error: %v\n", err)
			os.Exit(1)
		}
	}
}
