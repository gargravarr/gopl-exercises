package main

import (
	"fmt"
	"unicode"
	"unicode/utf8"
)

const asciiSpace = ' '

func main() {
	unicodeStrs := [][]byte{
		[]byte("Rodas Manel"),
		[]byte("Rodas               Maria"),
		[]byte("RodasManel"),
		[]byte("Rodas     Maria　Manuela"),
	}

	for _, str := range unicodeStrs {
		str = squashSpaces(str)
		fmt.Printf("%q\n", string(str))
	}
}

func squashSpaces(s []byte) []byte {
	wasSpace := false
	i := 0

	for j := 0; j < len(s); {
		r, size := utf8.DecodeRune(s[j:])

		if !unicode.IsSpace(r) {
			if i == j {
				i += size

			} else {
				if wasSpace {
					i = utf8.EncodeRune(s[i:], ' ') + i
					wasSpace = false
				}
				i = utf8.EncodeRune(s[i:], r) + i
			}

		} else {
			wasSpace = true
		}

		j += size
	}

	return s[:i]
}
