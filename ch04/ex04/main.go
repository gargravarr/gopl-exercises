package main

import (
	"fmt"
)

func main() {
	s := []int{1, 2, 3, 4, 5}
	s = rotate(s, 3)
	fmt.Printf("%v\n", s)
}

func rotate(s []int, shift int) []int {
	shift %= len(s)
	if shift >= 0 {
		return append(s[len(s)-shift:], s[:len(s)-shift]...)
	}

	return append(s[-shift:], s[:-shift]...)
}
