package main

import "testing"

func TestRotate(t *testing.T) {
	cases := []struct {
		input []int
		shift int
		want  []int
	}{
		{
			[]int{1, 2, 3, 4, 5},
			0,
			[]int{1, 2, 3, 4, 5},
		},
		{
			[]int{1, 2, 3, 4, 5},
			5,
			[]int{1, 2, 3, 4, 5},
		},
		{
			[]int{1, 2, 3, 4, 5},
			7,
			[]int{4, 5, 1, 2, 3},
		},
		{
			[]int{1, 2, 3, 4, 5},
			-7,
			[]int{3, 4, 5, 1, 2},
		},
		{
			[]int{1, 2, 3, 4, 5},
			10,
			[]int{1, 2, 3, 4, 5},
		},
		{
			[]int{1, 2, 3, 4, 5},
			2,
			[]int{4, 5, 1, 2, 3},
		},
		{
			[]int{1, 2, 3, 4, 5},
			-1,
			[]int{2, 3, 4, 5, 1},
		},
		{
			[]int{1, 2, 3, 4, 5},
			-2,
			[]int{3, 4, 5, 1, 2},
		},
	}

	for _, c := range cases {
		res := rotate(c.input, c.shift)
		if len(res) != len(c.want) {
			t.Errorf("Different lengths %v: want %v\n", res, c.want)
			continue
		}

		for i, v := range res {
			if v != c.want[i] {
				t.Errorf("mismatch %v: want %v\n", res, c.want)
				break
			}
		}
	}
}
