// Rev reverses a string byte slice.
package main

import (
	"bufio"
	"fmt"
	"os"
	"unicode/utf8"
)

func main() {
	//!+array
	a := []byte("Rodrigo")
	b := []byte("罗德里戈")
	//	reverse(a[:])
	fmt.Println(len(a), len(b))
	//!-array

	a = reverse(a)
	b = reverse(b)

	fmt.Println(string(a))
	fmt.Println(string(b))

	// Interactive test of reverse.
	input := bufio.NewScanner(os.Stdin)
	for input.Scan() {
		bytes := []byte(input.Text())
		reverse(bytes)
		fmt.Printf("%q\n", bytes)
	}
	// NOTE: ignoring potential errors from input.Err()
}

// reverse reverses a byte string.
func reverse(a []byte) []byte {
	var b = make([]byte, len(a))

	for j, k := 0, len(a); j < len(a); {
		r, size := utf8.DecodeRune(a[j:])
		k -= size
		_ = utf8.EncodeRune(b[k:], r)
		j += size
	}

	return b
}
