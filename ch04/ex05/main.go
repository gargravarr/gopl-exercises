package main

import (
	"fmt"
)

func main() {
	s := []string{"xpto", "xpto", "xpto", "XPTO", "xpti", "xpti"}
	s = distinct(s)
	fmt.Printf("%s\n", s)
}

func distinct(strings []string) []string {
	if len(strings) < 2 {
		return strings
	}

	prev := strings[0]
	j := 1
	for i := 1; i < len(strings); i++ {
		if strings[i] != prev {
			strings[j] = strings[i]
			j++
		}

		prev = strings[i]
	}

	return strings[:j]
}
