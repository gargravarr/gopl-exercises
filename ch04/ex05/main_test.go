package main

import "testing"

func TestStripDup(t *testing.T) {
	cases := []struct {
		input string
		want  string
	}{
		{
			"Rodrigo",
			"Rodrigo",
		},
		{
			"Rodrigo   ",
			"Rodrigo",
		},
		{
			"Roodrigo",
			"Rodrigo",
		},
		{
			"Rooodriigo",
			"Rodrigo",
		},
	}

	for _, c := range cases {
		if res := delDup(c.input); res != c.want {
			t.Errorf("mismatch %q: want %q\n", res, c.want)
		}
	}
}
