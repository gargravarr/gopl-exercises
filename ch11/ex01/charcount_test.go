package ex01

import (
	"bufio"
	"fmt"
	"strings"
	"testing"
)

func TestCharcount(t *testing.T) {
	type result struct {
		count   map[rune]int
		utfLen  []int
		invalid int
	}

	tests := []struct {
		input string
		want  result
	}{
		{
			input: "Socorro ao mar",
			want: result{
				count: map[rune]int{
					'S': 1, 'o': 4, 'c': 1, 'r': 3, ' ': 2, 'a': 2, 'm': 1,
				},
				utfLen:  []int{0, 14, 0, 0, 0},
				invalid: 0,
			},
		},
		{
			input: "L'Été!",
			want: result{
				count: map[rune]int{
					'L': 1, '\'': 1, 'É': 1, 't': 1, 'é': 1, '!': 1,
				},
				utfLen:  []int{0, 4, 2, 0, 0},
				invalid: 0,
			},
		},
	}

	for _, test := range tests {
		in := bufio.NewReader(strings.NewReader(test.input))

		count, lens, inv := charcount(in)
		resMsg := fmt.Sprintf("charcount(%q): %v %v %d", test.input, count, lens, inv)

		if len(count) != len(test.want.count) {
			t.Error(resMsg)
		}

		for k, v := range count {
			if v != test.want.count[k] {
				t.Error(resMsg)
			}
		}

		if len(lens) != len(test.want.utfLen) {
			t.Error(resMsg)
		}

		for i, n := range lens {
			if n != test.want.utfLen[i] {
				t.Error(resMsg)
			}
		}

		if inv != test.want.invalid {
			t.Error(resMsg)
		}
	}
}
