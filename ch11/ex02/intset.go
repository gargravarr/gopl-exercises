package ex02

import (
	"bytes"
	"fmt"
)

// An integer set based on a built-in map.
type IntSet struct {
	vals map[int]bool
}

// Has reports whether the set contains the value x.
func (s *IntSet) Has(x int) bool {
	val, ok := s.vals[x]
	return ok && val
}

// Returns the number of elements
func (s *IntSet) Len() int {
	count := 0

	for _, val := range s.vals {
		if val {
			count++
		}
	}

	return count
}

// Add adds value x to the set
func (s *IntSet) Add(x int) {
	s.vals[x] = true
}

// AddAll adds all the values xs to the set.
func (s *IntSet) AddAll(xs ...int) {
	for _, val := range xs {
		s.Add(val)
	}
}

// Removes x from the set
func (s *IntSet) Remove(x int) {
	if val, ok := s.vals[x]; val && ok {
		s.vals[x] = false
	}
}

// Removes all elements from the set
func (s *IntSet) Clear() {
	s.vals = make(map[int]bool)
}

// Returns a copy of the set
func (s *IntSet) Copy() *IntSet {
	c := &IntSet{vals: make(map[int]bool)}

	for val, _ := range s.vals {
		c.vals[val] = s.vals[val]
	}

	return c
}

// UnionWith sets s to the union of s and t.
func (s *IntSet) UnionWith(t *IntSet) {
	for val, has := range t.vals {
		if has {
			s.Add(val)
		}
	}
}

// IntersectWith sets s to the intersection of s and t.
func (s *IntSet) IntersectWith(t *IntSet) {
	for val, has := range s.vals {
		if has && !t.Has(val) {
			s.Remove(val)
		}
	}
}

// DifferenceWith sets s to the difference of s and t.
func (s *IntSet) DifferenceWith(t *IntSet) {
	for val, has := range s.vals {
		if has && t.Has(val) {
			s.Remove(val)
		}
	}
}

// SymmetricDifferenceWith sets s to the symmetric difference of s and t.
func (s *IntSet) SymmetricDifferenceWith(t *IntSet) {
	if t.Len() == 0 {
		return
	}

	if s.Len() == 0 {
		s.AddAll(t.Elems()...)
		return
	}

	finalDiff := make(map[int]bool)

	for val, has := range s.vals {
		if has && !t.Has(val) {
			finalDiff[val] = true
		}
	}

	for val, has := range t.vals {
		if has && !s.Has(val) {
			finalDiff[val] = true
		}
	}

	s.vals = finalDiff
}

// Returns a slice with all the elements of s.
func (s *IntSet) Elems() []int {
	var elems []int

	for val, has := range s.vals {
		if has {
			elems = append(elems, val)
		}
	}

	return elems
}

// String returns the set as a string of the form "{1 2 3}".
func (s *IntSet) String() string {
	var buf bytes.Buffer
	buf.WriteByte('{')
	for val, has := range s.vals {
		if !has {
			continue
		}
		if buf.Len() > len("{") {
			buf.WriteByte(' ')
		}
		fmt.Fprintf(&buf, "%d", val)
	}
	buf.WriteByte('}')
	return buf.String()
}

//!-string
