// Copyright © 2016 Alan A. A. Donovan & Brian W. Kernighan.
// License: https://creativecommons.org/licenses/by-nc-sa/4.0/

package ex02

import (
	"gitlab.com/gargravarr/gopl-exercises/ch6/ex05"
	"strconv"
	"testing"
)

var (
	testsAdd = []struct {
		inputs []int
		want   []int
	}{
		{
			[]int{4},
			[]int{4},
		},
		{
			[]int{4, 4},
			[]int{4},
		},
		{
			[]int{4, 42},
			[]int{4, 42},
		},
		{
			[]int{4, 42, 42},
			[]int{4, 42},
		},
		{
			[]int{4, 42, 84, 168, 336, 672, 10394893849},
			[]int{4, 42, 84, 168, 336, 672, 10394893849},
		},
		{
			[]int{4, 42, 84, 168, 336, 672, 10394893849, 10394893849},
			[]int{4, 42, 84, 168, 336, 672, 10394893849},
		},
	}

	testsHas = []struct {
		initial []int
		input   int
		want    bool
	}{
		{
			[]int{},
			3,
			false,
		},
		{
			[]int{},
			10394893849,
			false,
		},
		{
			[]int{4, 42},
			3,
			false,
		},
		{
			[]int{4, 42},
			4,
			true,
		},
		{
			[]int{4, 42, 84, 168, 336, 672, 10394893849},
			41,
			false,
		},
		{
			[]int{4, 42, 84, 168, 336, 672, 10394893849},
			10394893849,
			true,
		},
	}

	testsLen = []struct {
		initial []int
		want    int
	}{
		{
			[]int{},
			0,
		},
		{
			[]int{4, 42},
			2,
		},
		{
			[]int{4, 42, 84, 168, 336, 672, 10394893849},
			7,
		},
	}

	testsRemove = []struct {
		initial []int
		input   int
		want    []int
	}{
		{
			[]int{},
			4,
			[]int{},
		},
		{
			[]int{4, 42},
			2,
			[]int{4, 42},
		},
		{
			[]int{4, 42},
			4,
			[]int{42},
		},
		{
			[]int{4, 42},
			4,
			[]int{42},
		},
		{
			[]int{4, 42, 84, 168, 336, 672, 10394893849},
			10394893849,
			[]int{4, 42, 84, 168, 336, 672},
		},
	}

	testsClear = []struct {
		initial []int
	}{
		{
			[]int{},
		},
		{
			[]int{4, 42},
		},
		{
			[]int{4, 42, 84, 168, 336, 672, 10394893849},
		},
	}

	testsCopyElems = []struct {
		initial []int
		want    []int
	}{
		{
			[]int{},
			[]int{},
		},
		{
			[]int{4, 42},
			[]int{4, 42},
		},
		{
			[]int{4, 42, 84, 168, 336, 672, 10394893849},
			[]int{4, 42, 84, 168, 336, 672, 10394893849},
		},
	}

	testsSetOps = []struct {
		initial       []int
		input         []int
		union         []int
		intersect     []int
		difference    []int
		symmetricDiff []int
	}{
		{
			[]int{},
			[]int{},
			[]int{},
			[]int{},
			[]int{},
			[]int{},
		},
		{
			[]int{},
			[]int{4, 42},
			[]int{4, 42},
			[]int{},
			[]int{},
			[]int{4, 42},
		},
		{
			[]int{4, 42},
			[]int{},
			[]int{4, 42},
			[]int{},
			[]int{4, 42},
			[]int{4, 42},
		},
		{
			[]int{4, 42},
			[]int{4},
			[]int{4, 42},
			[]int{4},
			[]int{42},
			[]int{42},
		},
		{
			[]int{4, 42},
			[]int{4, 42},
			[]int{4, 42},
			[]int{4, 42},
			[]int{},
			[]int{},
		},
		{
			[]int{4, 42},
			[]int{42, 84},
			[]int{4, 42, 84},
			[]int{42},
			[]int{4},
			[]int{4, 84},
		},
		{
			[]int{4, 42, 84, 168, 336},
			[]int{168, 336, 672, 10394893849},
			[]int{4, 42, 84, 168, 336, 672, 10394893849},
			[]int{168, 336},
			[]int{4, 42, 84},
			[]int{4, 42, 84, 672, 10394893849},
		},
	}

	testsString = []struct {
		initial []int
		want    string
	}{
		{
			[]int{},
			"{}",
		},
		{
			[]int{4, 42},
			"{4, 42}",
		},
		{
			[]int{4, 42, 84, 168, 336, 672, 10394893849},
			"{4 42 84 168 336 672 10394893849}",
		},
	}
)

func TestAddUintSet(t *testing.T) {

	for _, test := range testsAdd {
		set := &ex05.IntSet{}

		for _, in := range test.inputs {
			set.Add(in)
		}
		if set.Len() != len(test.want) {
			t.Errorf("set.Add(%v): got %q", test.inputs, set)
		}

		for _, val := range test.want {
			if !set.Has(val) {
				t.Errorf("set.Add(%q): got %q", test.inputs, set)
			}
		}
	}
}

func TestAddMapSet(t *testing.T) {

	for _, test := range testsAdd {
		set := &IntSet{make(map[int]bool)}

		for _, in := range test.inputs {
			set.Add(in)
		}

		if set.Len() != len(test.want) {
			t.Errorf("set.Add(%v): got %q", test.inputs, set)
		}

		for _, val := range test.want {
			if !set.Has(val) {
				t.Errorf("set.Add(%q): got %q", test.inputs, set)
			}
		}
	}
}

func TestAddAllUintSet(t *testing.T) {

	for _, test := range testsAdd {
		set := &ex05.IntSet{}

		set.AddAll(test.inputs...)

		if set.Len() != len(test.want) {
			t.Errorf("set.Add(%v): got %q", test.inputs, set)
		}

		for _, val := range test.want {
			if !set.Has(val) {
				t.Errorf("set.Add(%q): got %q", test.inputs, set)
			}
		}
	}
}

func TestAddAllMapSet(t *testing.T) {

	for _, test := range testsAdd {
		set := &IntSet{make(map[int]bool)}

		set.AddAll(test.inputs...)

		if set.Len() != len(test.want) {
			t.Errorf("set.Add(%v): got %q", test.inputs, set)
		}

		for _, val := range test.want {
			if !set.Has(val) {
				t.Errorf("set.Add(%q): got %q", test.inputs, set)
			}
		}
	}
}

func TestHasUintSet(t *testing.T) {
	for _, test := range testsHas {
		set := &ex05.IntSet{}
		set.AddAll(test.initial...)

		got := set.Has(test.input)

		if got != test.want {
			t.Errorf("set.Has(%d): got %v", test.input, got)
		}
	}
}

func TestHasMapSet(t *testing.T) {
	for _, test := range testsHas {
		set := &IntSet{make(map[int]bool)}
		set.AddAll(test.initial...)

		got := set.Has(test.input)

		if got != test.want {
			t.Errorf("set.Has(%d): got %v", test.input, got)
		}
	}
}

func TestLenUintSet(t *testing.T) {
	for _, test := range testsLen {
		set := &ex05.IntSet{}
		set.AddAll(test.initial...)

		got := set.Len()

		if got != test.want {
			t.Errorf("set.Len(%v): got %d", test.initial, got)
		}
	}
}

func TestLenMapSet(t *testing.T) {
	for _, test := range testsLen {
		set := &IntSet{make(map[int]bool)}
		set.AddAll(test.initial...)

		got := set.Len()

		if got != test.want {
			t.Errorf("set.Len(%v): got %d", test.initial, got)
		}
	}
}

func TestRemoveUintSet(t *testing.T) {
	for _, test := range testsRemove {
		set := &ex05.IntSet{}
		set.AddAll(test.initial...)

		set.Remove(test.input)

		if set.Len() != len(test.want) {
			t.Errorf("set.Remove(%v): got %q", test.input, set)
		}

		for _, val := range test.want {
			if !set.Has(val) {
				t.Errorf("set.Remove(%q): got %q", test.input, set)
			}
		}
	}
}

func TestRemoveMapSet(t *testing.T) {
	for _, test := range testsRemove {
		set := &IntSet{make(map[int]bool)}
		set.AddAll(test.initial...)

		set.Remove(test.input)

		if set.Len() != len(test.want) {
			t.Errorf("set.Remove(%v): got %q", test.input, set)
		}

		for _, val := range test.want {
			if !set.Has(val) {
				t.Errorf("set.Remove(%q): got %q", test.input, set)
			}
		}
	}
}

func TestClearUintSet(t *testing.T) {
	for _, test := range testsClear {
		set := &ex05.IntSet{}
		set.AddAll(test.initial...)

		set.Clear()

		if set.Len() != 0 {
			t.Errorf("set.Clear(): got %d", set.Len())
		}
	}
}

func TestClearMapSet(t *testing.T) {
	for _, test := range testsClear {
		set := &IntSet{make(map[int]bool)}
		set.AddAll(test.initial...)

		set.Clear()

		if set.Len() != 0 {
			t.Errorf("set.Clear(): got %d", set.Len())
		}
	}
}

func TestCopyUintSet(t *testing.T) {
	for _, test := range testsCopyElems {
		set := &ex05.IntSet{}
		set.AddAll(test.initial...)

		got := set.Copy()

		if set.Len() != got.Len() {
			t.Errorf("set.Copy(): got %v", got)
		}

		for _, val := range got.Elems() {
			if !set.Has(val) {
				t.Errorf("set.Copy(): got %v", got)
			}
		}
	}
}

func TestCopyMapSet(t *testing.T) {
	for _, test := range testsCopyElems {
		set := &IntSet{make(map[int]bool)}
		set.AddAll(test.initial...)

		got := set.Copy()

		if set.Len() != got.Len() {
			t.Errorf("set.Copy(): got %v", got)
		}

		for _, val := range got.Elems() {
			if !set.Has(val) {
				t.Errorf("set.Copy(): got %v", got)
			}
		}
	}
}

func TestUnionUintSet(t *testing.T) {
	for _, test := range testsSetOps {
		set, inputSet := &ex05.IntSet{}, &ex05.IntSet{}
		set.AddAll(test.initial...)
		inputSet.AddAll(test.input...)

		set.UnionWith(inputSet)

		if set.Len() != len(test.union) {
			t.Errorf("set.UnionWith(%v): got %v", inputSet, set)
		}

		for _, val := range test.union {
			if !set.Has(val) {
				t.Errorf("set.UnionWith(%v): got %v", inputSet, set)
			}
		}
	}
}

func TestUnionMapSet(t *testing.T) {
	for _, test := range testsSetOps {
		set := &IntSet{make(map[int]bool)}
		inputSet := &IntSet{make(map[int]bool)}
		set.AddAll(test.initial...)
		inputSet.AddAll(test.input...)

		set.UnionWith(inputSet)

		if set.Len() != len(test.union) {
			t.Errorf("set.UnionWith(%v): got %v", inputSet, set)
		}

		for _, val := range test.union {
			if !set.Has(val) {
				t.Errorf("set.UnionWith(%v): got %v", inputSet, set)
			}
		}
	}
}

func TestIntersectUintSet(t *testing.T) {
	for _, test := range testsSetOps {
		set, inputSet := &ex05.IntSet{}, &ex05.IntSet{}
		set.AddAll(test.initial...)
		inputSet.AddAll(test.input...)

		set.IntersectWith(inputSet)

		if set.Len() != len(test.intersect) {
			t.Errorf("set.IntersectWith(%v): got %v", inputSet, set)
		}

		for _, val := range test.intersect {
			if !set.Has(val) {
				t.Errorf("set.IntersectWith(%v): got %v", inputSet, set)
			}
		}
	}
}

func TestIntersectMapSet(t *testing.T) {
	for _, test := range testsSetOps {
		set := &IntSet{make(map[int]bool)}
		inputSet := &IntSet{make(map[int]bool)}
		set.AddAll(test.initial...)
		inputSet.AddAll(test.input...)

		set.IntersectWith(inputSet)

		if set.Len() != len(test.intersect) {
			t.Errorf("set.IntersectWith(%v): got %v", inputSet, set)
		}

		for _, val := range test.intersect {
			if !set.Has(val) {
				t.Errorf("set.IntersectWith(%v): got %v", inputSet, set)
			}
		}
	}
}

func TestDifferenceUintSet(t *testing.T) {
	for _, test := range testsSetOps {
		set, inputSet := &ex05.IntSet{}, &ex05.IntSet{}
		set.AddAll(test.initial...)
		inputSet.AddAll(test.input...)

		set.DifferenceWith(inputSet)

		if set.Len() != len(test.difference) {
			t.Errorf("set.DifferenceWith(%v): got %v", inputSet, set)
		}

		for _, val := range test.difference {
			if !set.Has(val) {
				t.Errorf("set.DifferenceWith(%v): got %v", inputSet, set)
			}
		}
	}
}

func TestDifferenceMapSet(t *testing.T) {
	for _, test := range testsSetOps {
		set := &IntSet{make(map[int]bool)}
		inputSet := &IntSet{make(map[int]bool)}
		set.AddAll(test.initial...)
		inputSet.AddAll(test.input...)

		set.DifferenceWith(inputSet)

		if set.Len() != len(test.difference) {
			t.Errorf("set.DifferenceWith(%v): got %v", inputSet, set)
		}

		for _, val := range test.difference {
			if !set.Has(val) {
				t.Errorf("set.DifferenceWith(%v): got %v", inputSet, set)
			}
		}
	}
}

func TestSymmetricDiffUintSet(t *testing.T) {
	for _, test := range testsSetOps {
		set, inputSet := &ex05.IntSet{}, &ex05.IntSet{}
		set.AddAll(test.initial...)
		inputSet.AddAll(test.input...)

		set.SymmetricDifferenceWith(inputSet)

		if set.Len() != len(test.symmetricDiff) {
			t.Errorf("set.SymmetricDifferenceWith(%v): got %v", inputSet, set)
		}

		for _, val := range test.symmetricDiff {
			if !set.Has(val) {
				t.Errorf("set.SymmetricDifferenceWith(%v): got %v", inputSet,
					set)
			}
		}
	}
}

func TestSymmetricDiffMapSet(t *testing.T) {
	for _, test := range testsSetOps {
		set := &IntSet{make(map[int]bool)}
		inputSet := &IntSet{make(map[int]bool)}
		set.AddAll(test.initial...)
		inputSet.AddAll(test.input...)

		set.SymmetricDifferenceWith(inputSet)

		if set.Len() != len(test.symmetricDiff) {
			t.Errorf("set.SymmetricDifferenceWith(%v): got %v", inputSet, set)
			t.Errorf("set.SymmetricDifferenceWith(%d): got %d", set.Len(), len(test.symmetricDiff))
		}

		for _, val := range test.symmetricDiff {
			if !set.Has(val) {
				t.Errorf("set.SymmetricDifferenceWith(%v): got %v", inputSet,
					set)
			}
		}
	}
}

func TestElemsUintSet(t *testing.T) {
	for _, test := range testsCopyElems {
		set := &ex05.IntSet{}
		set.AddAll(test.initial...)

		got := set.Elems()

		if set.Len() != len(got) {
			t.Errorf("set.Elems(): got %v", got)
		}

		for _, val := range got {
			if !set.Has(val) {
				t.Errorf("set.Elems(): got %v", got)
			}
		}
	}
}

func TestElemsMapSet(t *testing.T) {
	for _, test := range testsCopyElems {
		set := &IntSet{make(map[int]bool)}
		set.AddAll(test.initial...)

		got := set.Elems()

		if set.Len() != len(got) {
			t.Errorf("set.Elems(): got %v", got)
		}

		for _, val := range got {
			if !set.Has(val) {
				t.Errorf("set.Elems(): got %v", got)
			}
		}
	}
}

func TestStringUintSet(t *testing.T) {
	for _, test := range testsString {
		set := &ex05.IntSet{}
		set.AddAll(test.initial...)

		got := set.String()

		if !isStringEquivalent(test.initial, got) {
			t.Errorf("%v set.String(): got %v", test.initial, got)
		}
	}
}

func TestStringMapSet(t *testing.T) {
	for _, test := range testsString {
		set := &IntSet{make(map[int]bool)}
		set.AddAll(test.initial...)

		got := set.String()

		if !isStringEquivalent(test.initial, got) {
			t.Errorf("%v set.String(): got %v", test.initial, got)
		}
	}
}

func isStringEquivalent(initial []int, got string) bool {
	runes := []rune(got)

	var digits []rune
	for i, runeVal := range runes {
		switch runeVal {

		case '{':
			if i != 0 {
				return false
			}

		case '}':
			if i != len(runes)-1 {
				return false
			}
			fallthrough

		case ' ':
			// Don't convert on empty set
			if i != 1 {
				num, err := strconv.Atoi(string(digits))

				if err != nil {
					return false

				} else {
					for j, numInitial := range initial {
						if num == numInitial {
							digits = nil
							break
						} else if j == len(initial)-1 {
							return false
						}
					}
				}
			}

		default:
			digits = append(digits, runeVal)
		}
	}

	return true
}
