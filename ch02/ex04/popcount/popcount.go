package popcount

// PopCount returns the population count (number of set bits) of x.
func PopCount(x uint64) int {
	res := 0

	for i := 0; i < 64; i++ {
		res += x & 1
		x >>= 1
	}

	return res
}
