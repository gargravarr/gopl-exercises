// Cf converts its numeric argument to Celsius and Fahrenheit, Meters
// and Feet, and Kilos and Pounds.
package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"

	"gitlab.com/gargravarr/gopl-exercises/ch02/ex02/distconv"
	"gitlab.com/gargravarr/gopl-exercises/ch02/ex02/weightconv"
	"gopl.io/ch2/tempconv"
)

func main() {
	args := os.Args[1:]
	if len(args) == 0 {
		scanner := bufio.NewScanner(os.Stdin)
		for scanner.Scan() {
			convAndPrint(scanner.Text())
		}

		if err := scanner.Err(); err != nil {
			fmt.Fprintf(os.Stderr, "cf: %v\n", err)
			os.Exit(1)
		}

		return
	}

	for _, arg := range args {
		convAndPrint(arg)
	}
}

func convAndPrint(arg string) {
	n, err := strconv.ParseFloat(arg, 64)
	if err != nil {
		fmt.Fprintf(os.Stderr, "cf: %v\n", err)
		os.Exit(1)
	}

	f := tempconv.Fahrenheit(n)
	c := tempconv.Celsius(n)
	m := distconv.Meters(n)
	ft := distconv.Feet(n)
	kg := weightconv.Kilos(n)
	lb := weightconv.Pounds(n)

	fmt.Printf("%s = %s, %s = %s, %s = %s, %s = %s, %s = %s, %s = %s\n",
		f, tempconv.FToC(f), c, tempconv.CToF(c), m, distconv.MToF(m), ft,
		distconv.FToM(ft), kg, weightconv.KgToLb(kg), lb, weightconv.LbToKg(lb))
}
