// Package weightconv performs kilos and pounds conversions.
package weightconv

import "fmt"

type Kilos float64
type Pounds float64

func (kg Kilos) String() string  { return fmt.Sprintf("%gkg", kg) }
func (lb Pounds) String() string { return fmt.Sprintf("%glb", lb) }
