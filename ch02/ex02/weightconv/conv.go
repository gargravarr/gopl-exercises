package weightconv

// KgToLb converts a weight in kilograms to pounds.
func KgToLb(kg Kilos) Pounds { return Pounds(kg * 2.2046) }

// LbToKg converts a weight in pounds to kilograms.
func LbToKg(lb Pounds) Kilos { return Kilos(lb / 2.2046) }

//!-
