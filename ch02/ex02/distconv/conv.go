package distconv

// MToF converts a meters distance to feet.
func MToF(m Meters) Feet { return Feet(m * 3.281) }

// FToM converts a feet distance to meters.
func FToM(f Feet) Meters { return Meters(f / 3.281) }

//!-
